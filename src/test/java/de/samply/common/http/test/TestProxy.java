/*
 * Copyright (C) 2019 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.common.http.test;

import de.samply.common.http.HttpConnector;
import java.util.HashMap;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class TestProxy {

  public static void main(String[] args) {
    HashMap<String, String> config = new HashMap<>();
    config.put(HttpConnector.PROXY_HTTP_HOST, "http://172.19.0.2");
    config.put(HttpConnector.PROXY_HTTP_PORT, "3128");
    config.put(HttpConnector.PROXY_HTTPS_HOST, "http://172.19.0.2");
    config.put(HttpConnector.PROXY_HTTPS_PORT, "3128");

    try {
      // Initiate the connector
      HttpConnector httpConnector = new HttpConnector(config);

      // To get a prepared Jersey client for a certain url
      Client jerseyClient = httpConnector.getJerseyClient("https://mdr.miracum.org");
      WebTarget target = jerseyClient.target(
          "https://mdr.miracum.org/rest/api/mdr/dataelements/urn:miracum:dataelement:112:1/catalog/code?codeValue=H40");
      Invocation.Builder invocationBuilder = target.request(MediaType.APPLICATION_XML);
      Response response = invocationBuilder.get();

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
