# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [5.2.1 - 2022-02-23]
### Security
- updated SLF4J to 1.7.36 (CVE-2018-8088)
- updated common-config (Log4j) to 3.0.4 (CVE-2021-45105, CVE-2021-44832, CVE-2021-45046)
- updated jersey to 2.35
- updated JSON provider to 2.13.1

## [5.2.0 - 2019-09-27]
### Fixed
- proxy server starting with "http[s]://" no longer causes errors in some cases

## [5.1.0 - 2019-09-26]
### Fixed
- proxy server is used with jersey 2 client

## [5.0.0 - 2018-12-07]
### Changed
- Migrate from Jersey 1 to Jersey 2
- New samply.common.config version 3.0.0


## [4.0.0 - 2018-01-08]
### Added
- Add option to add custom default headers
### Removed
- Removed redundant throws clauses
